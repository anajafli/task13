package Task1;
public class Main {
    public static void main(String[] args) {
        Car car = new Car("Toyota", "Camry", 2022, 50.0);

        String carMake = car.getMake();
        System.out.println("Car make: " + carMake);

        car.setRentalPrice(60.0);
        double carRentalPrice = car.getRentalPrice();
        System.out.println("Car rental price: " + carRentalPrice);
    }
}
