package Task3;

public class Main {
    public static void main(String[] args) {
        AudioPlayer audioPlayer = new AudioPlayer();
        VideoPlayer videoPlayer = new VideoPlayer();

        audioPlayer.play();
        audioPlayer.stop();

        System.out.println();

        videoPlayer.play();
        videoPlayer.stop();
    }
}