package Task3;

interface Playable {
    void play();
    void stop();
}

class AudioPlayer implements Playable {
    public void play() {
        System.out.println("Audio player: Playing audio");
    }

    public void stop() {
        System.out.println("Audio player: Stopping audio");
    }
}

class VideoPlayer implements Playable {
    public void play() {
        System.out.println("Video player: Playing video");
    }

    public void stop() {
        System.out.println("Video player: Stopping video");
    }
}


