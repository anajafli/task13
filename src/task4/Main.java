package task4;

public class Main {
    public static void main(String[] args) {
        EconomyCar economyCar = new EconomyCar("FORD", "Fusion", 2015, 40.0, 30.0);
        LuxuryCar luxuryCar = new LuxuryCar("BMW", "F10", 2022, 200.0, new String[]{"Leather Seats", "Premium Sound System"});

        RentalTransaction rentalTransaction1 = new RentalTransaction(economyCar, "John Doe", 5);
        RentalTransaction rentalTransaction2 = new RentalTransaction(luxuryCar, "Jane Smith", 7);

        rentalTransaction1.displayTransactionInfo();
        System.out.println();
        rentalTransaction2.displayTransactionInfo();
    }
}