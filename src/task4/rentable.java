package task4;

interface Rentable {
    void rent(int numDays);
    void returnCar();
}

abstract class Car implements Rentable {
    private String make;
    private String model;
    private int year;
    private double rentalRate;
    private boolean available;

    public Car(String make, String model, int year, double rentalRate) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.rentalRate = rentalRate;
        this.available = true;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRentalRate() {
        return rentalRate;
    }

    public void setRentalRate(double rentalRate) {
        this.rentalRate = rentalRate;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void rent(int numDays) {
        if (available) {
            System.out.println("Renting the car for " + numDays + " days.");
            available = false;
        } else {
            System.out.println("Sorry, the car is already rented.");
        }
    }

    public void returnCar() {
        if (!available) {
            System.out.println("Returning the car.");
            available = true;
        } else {
            System.out.println("The car is already available.");
        }
    }

    public abstract double calculateRentalCharge(int days);

    public void displayCarInfo() {
        System.out.println("Make: " + make);
        System.out.println("Model: " + model);
        System.out.println("Year: " + year);
        System.out.println("Rental Rate: " + rentalRate);
    }
}

class EconomyCar extends Car {
    private double fuelEfficiency;

    public EconomyCar(String make, String model, int year, double rentalRate, double fuelEfficiency) {
        super(make, model, year, rentalRate);
        this.fuelEfficiency = fuelEfficiency;
    }

    public double getFuelEfficiency() {
        return fuelEfficiency;
    }

    public void setFuelEfficiency(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
    }

    public double calculateRentalCharge(int days) {
        return getRentalRate() * days;
    }
}

class LuxuryCar extends Car {
    private String[] premiumFeatures;

    public LuxuryCar(String make, String model, int year, double rentalRate, String[] premiumFeatures) {
        super(make, model, year, rentalRate);
        this.premiumFeatures = premiumFeatures;
    }

    public String[] getPremiumFeatures() {
        return premiumFeatures;
    }

    public void setPremiumFeatures(String[] premiumFeatures) {
        this.premiumFeatures = premiumFeatures;
    }

    public double calculateRentalCharge(int days) {
        return getRentalRate() * days * 1.2; // 20% surcharge for luxury cars
    }
}

class RentalTransaction {
    private Car car;
    private String customerName;
    private int rentalDays;

    public RentalTransaction(Car car, String customerName, int rentalDays) {
        this.car = car;
        this.customerName = customerName;
        this.rentalDays = rentalDays;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getRentalDays() {
        return rentalDays;
    }

    public void setRentalDays(int rentalDays) {
        this.rentalDays = rentalDays;
    }

    public double calculateRentalCost() {
        return car.calculateRentalCharge(rentalDays);
    }

    public void displayTransactionInfo() {
        System.out.println("Car Details:");
        car.displayCarInfo();
        System.out.println("Customer Name: " + customerName);
        System.out.println("Rental Days: " + rentalDays);
        System.out.println("Rental Cost: " + calculateRentalCost());
    }
}


