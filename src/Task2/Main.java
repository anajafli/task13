package Task2;

public class Main {
    public static void main(String[] args) {
        Manager manager = new Manager("Jafar", 5000.0, "IT");
        Developer developer = new Developer("Afat", 2000.0, "Java");

        manager.getDetails();
        System.out.println();
        developer.getDetails();
    }
}